var argv = require("yargs").argv;
var gulp = require('gulp');
var runSequence = require('run-sequence');
var browserSync = require('browser-sync');
//var sourcemaps = require('gulp-sourcemaps');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var watchify = require('watchify');
var browserify = require('browserify');
var less = require('gulp-less');
var globby = require('globby');
var gutil = require('gulp-util');
var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var gulpif = require('gulp-if');
var changed = require('gulp-changed');
var preprocess = require('gulp-preprocess');
var rename = require('gulp-rename');
var del = require('del');
var angularTemplateCache = require('gulp-angular-templatecache');
var prodUglifyOptions = { mangle: true, compress: true};
var devUglifyOptions = { mangle: false, compress: false, output: { beautify: true }};
var uglifyOptions = (argv.prod ? prodUglifyOptions : devUglifyOptions);
var handleErrors = function(err) {
    gutil.beep();
    gutil.log(gutil.colors.bgRed('Error'), err.message);
    return this.end();
};

var appBundleName = "bundle.js";
var testBundleName = "testBundle.js";
var mockBundleName = "mockBundle.js";

var LINT_WATCH_FILES = ['!./app/modules/**/*_test.js', '!./app/bower_components/**/*.js',
    '!./app/bundle/'+appBundleName, '!./app/bundle/'+testBundleName,
    './app/*.js', './app/config/**/*.js', './app/modules/**/*.js'];
var LESS_WATCH_FILES = ['./app/less/app.less', './app/components/**/*.less', './app/modules/**/*.less',
    './app/less/singletons.less', './app/less/app-*.less'];
var LESS_BUILD_FILES= ['./app/less/app.less', './app/components/**/*.less', './app/modules/**/*.less',
    './app/less/singletons.less', '!./app/less/app-*.less', '!./app/bower_components/**/*.less'];
var TEMPLATE_WATCH_FILES = ['!./app/index.html', '!./app/mock.html', '!./app/mock-backend/**/*.html',
    './app/modules/**/*.html'];
var MOVE_ONLY_FOR_MOCK_FILES = ['!./bower_components/angular-mocks/**/*',
    '!./bower_components/angular-mock-back/**/*'];
var LINT_FOR_MOCK_FILES = ['./app/mock-backend/mock-data/**/*.js',
    '!./app/mock-backend/mock-data/bundle/'+mockBundleName];
var BOWER_FILES = ['./app/bower_components/**/*'];
var BUNDLER_FILES = ['./app/app.js'];
var MOCK_BUNDLER_FILES = ['./app/mock-backend/mock-data/**/*.js',
    '!./app/mock-backend/mock-data/bundle/' + mockBundleName];
if(argv.mockBackend){
    LINT_WATCH_FILES = LINT_WATCH_FILES.concat(LINT_FOR_MOCK_FILES);
}else{
    BOWER_FILES = BOWER_FILES.concat(MOVE_ONLY_FOR_MOCK_FILES);
}
if(argv.mobile){
    BUNDLER_FILES.push('./app/mobile.js');
    LESS_WATCH_FILES.push('./app/less/mobile.less');
    LESS_BUILD_FILES.push('./app/less/mobile.less');
}

var appBundler = browserify(BUNDLER_FILES, watchify.args);
function rebundle() {
    console.log("Bundling");

    return appBundler.bundle()
        .pipe(source(appBundleName))
        .pipe(buffer())
        //.pipe(gulpif(argv.sourceMaps, sourcemaps.init({loadMaps: true})))
        //.pipe(gulpif(argv.sourceMaps, sourcemaps.write('./')))
        .pipe(uglify(uglifyOptions))
        .on('error', handleErrors)
        .pipe(gulp.dest('./app/bundle'));
}
var mockDataFiles = globby.sync(MOCK_BUNDLER_FILES);
var mockBundler = browserify(mockDataFiles, watchify.args);
function rebundleMocks() {
    console.log("Bundling Mocks");

    return mockBundler
        .bundle()
        .pipe(source(mockBundleName))
        .pipe(gulp.dest('./app/mock-backend/mock-data/bundle'));
}

// Combines all template (HTML) files of the app into a single Angular module which adds files to the angular template cache
gulp.task('templates', function () {
    console.log("Building Template Cache");

    gulp.src(TEMPLATE_WATCH_FILES)
        .pipe(angularTemplateCache('templates.js', {
            standalone: true,
            module: 'app.templates'
        }))
        .pipe(gulp.dest('./app/config'));
});

gulp.task('bundler', function () {
    console.log("Building Bundle");
    return rebundle();
});

gulp.task('bundler-watch', function () {
    console.log("Watching Bundle");
    appBundler = watchify(appBundler);
    appBundler.on('update', rebundle);
    return rebundle();
});

gulp.task('watchify-tests', function() {
    var testFiles = globby.sync(['./app/**/*_test.js', '!./app/bundle/' + testBundleName]);

    var watchified = watchify(browserify(testFiles, watchify.args));

    function bundleWatched() {
        return watchified
            .bundle()
            .pipe(source(testBundleName))
            .pipe(gulp.dest('./app/bundle'));
    }

    watchified.on('update', bundleWatched);
    watchified.on('time', function (time) {
        gutil.log('Watchify', gutil.colors.cyan("'tests'"), 'after', gutil.colors.magenta(time),
            gutil.colors.magenta('ms'));
    });

    return bundleWatched();
});

gulp.task('mock-bundler', function () {
    console.log("Building Bundle");
    if(argv.mockBackend){
        return rebundleMocks();
    }
});

gulp.task('mock-bundler-watch', function () {
    console.log("Watching Mock Bundle");
    mockBundler = watchify(browserify(mockDataFiles, watchify.args));
    mockBundler.on('update', rebundleMocks);
    return rebundleMocks();
});

gulp.task('less-css', function () {
    console.log('Building CSS from Less');
    // compile the LESS files
    gulp.src(['./app/less/app.less',
              './app/components/**/*.less',
              './app/modules/**/*.less',
              './app/less/singletons.less',
              '!./app/less/app-*.less',
              '!./app/bower_components/**/*.less'])
        //.pipe(gulpif(argv.sourceMaps, sourcemaps.init({loadMaps: true})))
        //.pipe(gulpif(argv.sourceMaps, sourcemaps.write('./')))
        .pipe(concat('app.less'))
        .pipe(less({
            compress: (argv.prod ? true : false),
            yuicompress: (argv.prod ? true : false),
            strictMath: 'on',
            relativeUrls: false
        }))
        .pipe(gulp.dest('./dist/css'));
});

// Runs JSHint Report against all JS files in app
gulp.task('lint', function () {
    return gulp.src(LINT_WATCH_FILES)
               .pipe(jshint())
               .pipe(jshint.reporter('jshint-stylish'))
               .pipe(jshint.reporter('fail'));
});

/**
 * Build Tasks
 */

gulp.task('mock-backend-html', function() {
    if(argv.mockBackend){
        console.log("Creating mockBackend html");
        return gulp.src('./app/index.html')
            .pipe(preprocess({
                context: { mockBackend: argv.mockBackend }
            }))
            .pipe(rename('mock.html'))
            .pipe(gulp.dest('./app'));
    }
});

gulp.task('watch-lint', function () {
    console.log("Watching Lint");

    // Lint the JS files when they change
    gulp.watch(LINT_WATCH_FILES, ['lint']);
});

gulp.task('watch-templates', function () {
    console.log("Watching Templates");

    // Rebuild the template cache when any HTML file changes
    gulp.watch(TEMPLATE_WATCH_FILES, ['templates']);
});

gulp.task('watch-less', function () {
    console.log("Watching Less");

    gulp.watch(LESS_WATCH_FILES, ['less-css']);
});

gulp.task('watch-bundle', function () {
    console.log("Watching Bundle");

    gulp.watch('./app/bundle/'+appBundleName, ['copy-bundle']);
});

gulp.task('watch-images', function () {
    console.log("Watching Images");

    gulp.watch('./app/img/**/*', ['copy-img']);
});

gulp.task('watch-index', function () {
    console.log("Watching Index");

    gulp.watch('./app/index.html', ['copy-index']);
});

gulp.task('watch-mock-back', function () {
    console.log("Watching Mock Back");

    gulp.watch(['./app/index.html', './app/mock-backend/**/*','./app/mock-backend/mock-data/'+mockBundleName],
        ['copy-mock-back']);
});


gulp.task('copy-watch-resources',['copy'], function (done) {
    console.log("Watching and Copying Resources");

    runSequence(['watch-lint', 'watch-templates', 'watch-less', 'watch-bundle', 'watch-images', 'watch-index',
            'watch-mock-back'], done);
});

gulp.task('copy', ['copy-bower', 'copy-mock-back', 'copy-index', 'copy-bundle', 'copy-img']);

gulp.task('copy-index', function () {
    if(argv.mockBackend && argv.mobile){
        console.log("Copying Index is not necessary when mockBackend and mobile args are true");
    }else{
        console.log("Copying Index");
        gulp.src('./app/index.html')
            .pipe(gulp.dest('./dist'));
    }
});

gulp.task('copy-bundle', function () {
    console.log("Copying Bundle");
    gulp.src(['./app/bundle/'+appBundleName])
        .pipe(gulpif(argv.prod, uglify()))
        //.pipe(changed('./dist/bundle/'+appBundleName))
        .on('error', handleErrors)
        .pipe(gulp.dest('./dist/bundle'));
});

gulp.task('copy-img', function () {
    console.log("Copying Images");
    gulp.src(['./app/img/**/*'])
        //.pipe(changed('./dist/img'))
        .pipe(gulp.dest('./dist/img'));
});

gulp.task('copy-bower', function () {
    console.log("Copying Bower Files");
    gulp.src(BOWER_FILES)
        //.pipe(changed('./dist/bower_components'))
        .on('error', handleErrors)
        .pipe(gulp.dest('./dist/bower_components'));
});

gulp.task('copy-mock-back', ['mock-backend-html'], function () {
    console.log("Copying Mock Back Files");
    if(argv.mockBackend){
        if(argv.mobile){
            del(['./dist/mock.html'],
                function (err, deletedFiles) {
                    console.log('Files deleted because running mockBackend for mobile:', deletedFiles.join(', '));
                });
            gulp.src('./app/mock.html')
                .pipe(rename('index.html'))
                .pipe(gulp.dest('./dist'));
        }else{
            gulp.src(['./app/mock-backend/mock-data/bundle/'+mockBundleName])
                .pipe(gulpif(argv.prod, uglify()))
                //.pipe(changed('./dist/bundle/'+appBundleName))
                .on('error', handleErrors)
                .pipe(gulp.dest('./dist/mock-backend/mock-data/bundle/'));
            gulp.src(['./app/mock.html'])
                //.pipe(changed('./dist/mock.html'))
                .pipe(gulp.dest('./dist'));
        }
        gulp.src(['./app/mock-backend/**/*'])
            //.pipe(changed('./dist/mock-backend'))
            .pipe(gulp.dest('./dist/mock-backend'));
    }else{
        console.log
        del(['./dist/mock-backend',
                './dist/bower_components/angular-mock*',
                './dist/mock.html'],
            function (err, deletedFiles) {
                console.log('Files deleted because running without mockBackend:', deletedFiles.join(', '));
            });
    }
});

gulp.task('browser-sync', function () {
    console.log("Loading browserSync");
    browserSync({
        server: {
            baseDir: ['./dist']
        },
        files: ['./dist/**'],
        port: 9000, // keep consistent with express
        open: false // stop the browser from opening automatically
    }, function (err) {
        if (err) {
            console.log("BrowserSync Error");
            handleErrors(err);
        }else{
            console.log("BrowserSync is ready!");
        }
    });
});

gulp.task('clean', function (cb) {
    console.log("Cleaning Dist");
    del(['./dist'],
        function (err, deletedFiles) {
            console.log('Files deleted because running with clean:', deletedFiles.join(', '));
        });
});

// Main build task.  Runs all build-related tasks
gulp.task('build', function (done) {
    // Run Sequence runs tasks before the array synchronously, then runs the tasks in the array asynchronously, then runs tasks after the array synchronously
    runSequence(['lint', 'less-css', 'templates'], 'bundler', 'mock-bundler', 'copy',
        done);
});

gulp.task('default', function(done) {
    runSequence(['lint', 'less-css', 'templates'], 'bundler-watch', 'mock-bundler-watch', 'watchify-tests',
        'copy-watch-resources', 'browser-sync',
        done);
});
