/* global angular, module, require */
'use strict';

/*
 *
 */
module.exports = ['$translateProvider', function ($translateProvider) {
    
  $translateProvider.translations('en', require('../languages/en'));

  $translateProvider.preferredLanguage('en');
}];