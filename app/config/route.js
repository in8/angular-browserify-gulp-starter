/* global angular, module, require */
'use strict';

/**
 *
 */
module.exports = ['$stateProvider','$urlRouterProvider', 'DEMO_TEMPLATE',
      function($stateProvider, $urlRouterProvider, DEMO_TEMPLATE) {

  $urlRouterProvider.otherwise("/demo");

  $stateProvider
    
    .state("demo", {
      url: "/demo",
      template: DEMO_TEMPLATE,
      controller: 'DemoController as demoCtrl',
      resolve: {
        demoApiData: function (DemoApiService) {
          return DemoApiService.getDemoResults();
        }
      }
    });

}];