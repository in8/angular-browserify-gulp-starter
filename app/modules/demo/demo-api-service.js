/* global define, require, _ */
'use strict';

function DemoApiService($q, $http, uriConfig) {
    var demoApiService = this;

    demoApiService.demoResults = {};

    demoApiService.formatResponse = function (rawResponse) {
        var parsed = rawResponse;

        if (typeof rawResponse === "string") {
            parsed = JSON.parse(rawResponse);
        }

        return parsed;
    };

    demoApiService.getDemoResults = function () {
        var deferred = $q.defer();
        $http({
            method: "GET",
            url: uriConfig.BASE_SERVICE_URL + "/demo",
            transformResponse: demoApiService.formatResponse
        }).success(function (data) {
            // console.log("Server Response", data);
            demoApiService.demoResults = data;
            deferred.resolve(data);
        });
        return deferred.promise;
    };

}

module.exports = ['$q', '$http', 'uriConfig', DemoApiService];