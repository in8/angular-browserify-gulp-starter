/* global angular, module, require */
'use strict';

var demoTemplate = require('./demo.html'),
    demoController = require('./demo-controller'),
    demoApiService = require('./demo-api-service');

module.exports = angular.module('app.demo', [])
    .constant('DEMO_TEMPLATE', demoTemplate)
    .controller('DemoController', demoController)
    .service('DemoApiService', demoApiService);
