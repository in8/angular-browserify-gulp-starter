/* global angular, module, require */
describe("app.demo", function () {
    // declare variables needed across methods
    var demoCtrl,
        demoApiData,
        scope;

    beforeEach(angular.mock.module('app.demo'));

    beforeEach(angular.mock.module(function ($provide) {

        demoApiData = {
            "hello": "hello test"
        };
        $provide.value('demoApiData', demoApiData);

    }));

    beforeEach(inject(function ($rootScope, $controller) {
        scope = $rootScope;
        demoCtrl = $controller('DemoController', {$scope: scope});
    }));
    // Recieves backend data (mocked)
    it('Should produce a failed test', function () {
        expect(demoCtrl.demoData).toBeNull();
    });
    it('Should produce a passed test', function () {
        expect(demoCtrl.demoData).not.toBeNull();
    });

});

