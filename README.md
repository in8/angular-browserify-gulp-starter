Starter Angular/Browserify/Gulp app to consume JSON api.

## Getting Started

Make sure you have Node (and NPM) and Bower installed.

`npm install -g bower` - This will install bower globally, so that you can run the postinstall script in the package.json

Install all dependencies (node, bower, gulp, etc...):

* `npm install`

Then, run gulp to start up the local server with BrowserSync:

* `npm run g:mb` - NOTE: This runs 'gulp --mockBackend', *with* BrowserSync and Watchers active, the '--mockBackend' option is necessary without an actual API.
* `npm run g:buildmb` - NOTE: This runs 'gulp build --mockBackend', *without* BrowserSync and Watchers active, the '--mockBackend' option is necessary without an actual API.

Other Gulp build options:

* `npm run g` - This runs gulp *with* Watchers and BrowserSync active, without args (no mocks)
* `npm run g:build` - This runs gulp build *without* Watchers and BrowserSync active, without args (no mocks)
* `npm run g:clean` - This runs gulp clean, which clears the `*/dist` directory
* `npm run g:mob` - This runs gulp clean, which clears the `*/dist` directory
* `npm run g:mbmob` - NOTE: This runs 'gulp --mockBackend --mobile', *with* BrowserSync and Watchers active, the '--mockBackend' option is necessary without an actual API, '--mobile' runs mobile specific tasks.
* `npm run g:buildmbmob` - NOTE: This runs 'gulp build --mockBackend --mobile', *without* BrowserSync and Watchers active, the '--mockBackend' option is necessary without an actual API, '--mobile' runs mobile specific tasks.

Navigate to [http://localhost:9000/mock.html](http://localhost:9000/mock.html) for BrowerserSync Mode